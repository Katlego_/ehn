#include "mixingRounds.h"
#include "constants.h"
#include "initialRound.h"
 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 
void subbytes(unsigned char word[][4])
{
	int i;
	int j;
	int temp;

	for(i = 0; i < 4; i++){
		for(j = 0; j < 4; j++){
			temp = word[i][j];
			word[i][j] = sBox[temp];
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void invSubbytes(unsigned char word[][4])
{
	int i;
	int j;
	int temp;

	for(i = 0; i < 4; i++){
		for(j = 0; j < 4; j++){
			temp = word[i][j];
			word[i][j] = inv_sBox[temp];
		}
	}
}
 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 
 void shiftRows(unsigned char word[][4])
{
	unsigned char temp; 
	 
	temp 		= word[3][0];
	word[3][0] 	= word[3][3];
	word[3][3] 	= word[3][2];
	word[3][2] 	= word[3][1];
	word[3][1] 	= temp;

	temp 		= word[2][0];
	word[2][0] 	= word[2][2];
	word[2][2] 	= temp;

	temp		= word[2][1];
	word[2][1] 	= word[2][3];
	word[2][3] 	= temp;

	temp 		= word[1][0];
	word[1][0] 	= word[1][1];
	word[1][1] 	= word[1][2];
	word[1][2] 	= word[1][3];
	word[1][3] 	= temp;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 
void invShiftRows(unsigned char word[][4])
{
	unsigned char temp;

	temp 		= word[3][1];
	word[3][1] 	= word[3][2];
	word[3][2] 	= word[3][3];
	word[3][3] 	= word[3][0];
	word[3][0] 	= temp;

	temp 		= word[2][3];
	word[2][3] 	= word[2][1];
	word[2][1] 	= temp;
	
	temp 		= word[2][2];
	word[2][2] 	= word[2][0];
	word[2][0] 	= temp;

	temp 		= word[1][3];
	word[1][3] 	= word[1][2];
	word[1][2] 	= word[1][1];
	word[1][1] 	= word[1][0];
	word[1][0] 	= temp;	
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 
unsigned char gmul(unsigned char x, unsigned char y)
{
	int i;
	int bit;
	
	unsigned char temp = 0;

	for(i = 0; i < 8; i++) {
		if((y & 1) != 0){
			temp ^= x;
		}
		
		bit = (x & 0x80) != 0;
		x <<= 1;
		
		if(bit){
			x ^= 0x1b;
		}
		
		y >>= 1;
	}

	return temp;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void mixColumns(unsigned char word[][4])
{
	int i;
	int j;

	unsigned char temp[4][4];

	for(i = 0; i < 4; i++){
		temp[0][i] = gmul(0x02, word[0][i]) ^ gmul(0x03, word[1][i]) 	^ word[2][i] 			^ word[3][i];
		temp[1][i] = word[0][i] 			^ gmul(0x02, word[1][i]) 	^ gmul(0x03, word[2][i]) 	^ word[3][i];
		temp[2][i] = word[0][i] 			^ word[1][i] 			^ gmul(0x02, word[2][i]) 	^ gmul(0x03, word[3][i]);
		temp[3][i] = gmul(0x03, word[0][i]) ^ word[1][i] 			^ word[2][i] 			^ gmul(0x02, word[3][i]);
	}
	
	for(i = 0; i < 4; i++){
		for(j =0; j < 4; j++){
			word[i][j] = temp[i][j];	
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void invMixColumns(unsigned char word[][4])
{
	int i;
	int j;

	unsigned char temp[4][4];

	for(i = 0; i < 4; i++){
		for(j =0; j < 4; j++){
			temp[i][j] = word[i][j];	
		}
	}
	
	for(i = 0; i < 4; i++){
		word[0][i] = gmul(0x0E, temp[0][i]) ^ gmul(0x0B, temp[1][i])	^ gmul(0x0D, temp[2][i])	^ gmul(0x09, temp[3][i]);
		word[1][i] = gmul(0x09, temp[0][i]) ^ gmul(0x0E, temp[1][i])	^ gmul(0x0B, temp[2][i])	^ gmul(0x0D, temp[3][i]);
		word[2][i] = gmul(0x0D, temp[0][i]) ^ gmul(0x09, temp[1][i])	^ gmul(0x0E, temp[2][i])	^ gmul(0x0B, temp[3][i]);
		word[3][i] = gmul(0x0B, temp[0][i]) ^ gmul(0x0D, temp[1][i])	^ gmul(0x09, temp[2][i])	^ gmul(0x0E, temp[3][i]);
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void decryptMixingRounds(unsigned char word[][4], unsigned char keyExpanded[240], int k)
{
	invShiftRows(word);
	invSubbytes(word);
	addRoundkey(word, k, keyExpanded);
	invMixColumns(word);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int encryptMixingRounds(unsigned char word[][4], unsigned char keyExpanded[240], int k)
{
	subbytes(word);
	shiftRows(word);
	mixColumns(word);

	return encryptAddRoundkey(word, keyExpanded, k);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////