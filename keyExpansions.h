#ifndef KEYEXPANSIONS_H
#define KEYEXPANSIONS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void convert(char enteredKey[], char key[], int size);

void wordRotation(char word[4]);

void coreKeyScheduler(unsigned char word[4], unsigned char RCON);

void keyExpansion(unsigned char key[32], unsigned char keyExpanded[240], int size);

#endif