#ifndef PRINT_H
#define PRINT_H

void print32Bits(unsigned char word[][4]);

void printWord(unsigned char word[4]);

void printKey(unsigned char key[240], int size);

#endif