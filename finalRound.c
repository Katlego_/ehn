#include "finalRound.h"
#include "initialRound.h"
#include "mixingRounds.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void decryptFinalRound(unsigned char word[][4], unsigned char keyExpanded[240])
{
	invShiftRows(word);
	invSubbytes(word);
	addRoundkey(word, 0, keyExpanded);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int encryptFinalRound(unsigned char word[][4], unsigned char keyExpanded[240], int k)
{
	shiftRows(word);
	subbytes(word);
	
	return encryptAddRoundkey(word, keyExpanded, k);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////