#ifndef INITIALROUND_H
#define INITIALROUND_H

void addRoundkey(unsigned char word[][4], int round, unsigned char keyExpanded[240]);

int encryptAddRoundkey(unsigned char word[][4], unsigned char keyExpanded[240], int k);

#endif