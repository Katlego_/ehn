#ifndef MIXINGROUNDS_H
#define MIXINGROUNDS_H

void subbytes(unsigned char word[][4]);

void invSubbytes(unsigned char word[][4]);

void shiftRows(unsigned char word[][4]);

void invShiftRows(unsigned char word[][4]);

unsigned char gmul(unsigned char x, unsigned char y);

void mixColumns(unsigned char word[][4]);

void invMixColumns(unsigned char word[][4]);

void decryptMixingRounds(unsigned char word[][4], unsigned char keyExpanded[240], int k);

int encryptMixingRounds(unsigned char word[][4], unsigned char keyExpanded[240], int k);

#endif