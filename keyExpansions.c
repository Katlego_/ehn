#include "keyExpansions.h"
#include "constants.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void convert(char enteredKey[], char key[], int size){
	int i = 0;

	for (i = 0; i < size; i++){
		unsigned char tempCharKey[] = {"0x00"};

		tempCharKey[2] = enteredKey[i*2];
		tempCharKey[3] = enteredKey[i*2 + 1];

		key[i] = (int) strtol(tempCharKey, NULL, 16);
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*32-bit word rotation*/

void wordRotation(char word[4])
{
	unsigned char temp = word[0];
	
	strncpy(word, &word[1], strlen(word));
	word[3] = temp;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void coreKeyScheduler(unsigned char word[4], unsigned char RCON)
{
	int i;
	int temp = 0;

	wordRotation(word);

	for (i = 0; i < 4; i++){
		temp = word[i];
		word[i] = sBox[temp];
	}

	word[0] = word[0] ^ RCON;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void keyExpansion(unsigned char key[32], unsigned char keyExpanded[240], int size)
{
	int i 	= 0;
	int j 	= 0;
	int k 	= 0;
	int x 	= 0;
	
	int num 		= 0;
	int cntAES 	= 0;
	int RConVal 	= 1;
	
	int keySize;
	
	unsigned char temp1[4];
	unsigned char temp2[4];

	strncpy(keyExpanded, key, size);

	switch(size){
		case 16:
			keySize 	= 176;
			cntAES 	= 0;
			break;
		
		case 24:
			keySize 	= 208;
			cntAES	= 2;
			break;
		
		case 32:
			keySize 	= 240;
			cntAES 	= 3;
			break;
	}

	while (i < keySize){
		memset(temp1 , 0, strlen(temp1));
		memset(temp2 , 0, strlen(temp2));

		for (x = 0; x < 4; x++){
			temp1[x] = keyExpanded[i - (4 - x)];
		}

		coreKeyScheduler(temp1, RCON[RConVal]);
		RConVal++;

		for (x = 0; x < 4; x++){
			temp2[x] = temp1[x] ^ keyExpanded[i - size + x];
		}

		for (j =0; j < 4; j++){
			keyExpanded[i] = temp2[j];
			i++;
		}

		if (i == 240){
			break;
		}

		for (j = 0; j < 3; j++){
			for (x = 0; x < 4; x++){
				temp1[x] = keyExpanded[i - (4 - x)];
				temp2[x] = temp1[x] ^ keyExpanded[i - size + x];
			}

			for (k = 0; k < 4; k++){
				keyExpanded[i] = temp2[k];
				i++;
			}
			
			if (i == 240){
				break;
			}
		}
		
		if (i == 240){
			break;
		}

		if (size == 32){
			for (x = 0; x < 4; x++){
				temp1[x] = keyExpanded[i - (4 - x)];
			}

			for (k = 0; k < 4; k++){
				num = temp1[k];
				temp1[k] = sBox[num];
			}
			
			for (x = 0; x < 4; x++){
				temp2[x]	= temp1[x] ^ keyExpanded[i - size + x];
			}

			for (k = 0; k < 4; k++){
				keyExpanded[i] = temp2[k];
				i++; 
			}
			
			if (i == 240){
				break;
			}
		}
		
		if (i == 240){
			break;
		}

		for (j = 0; j < cntAES; j++){
			for (x = 0; x < 4; x++){
				temp1[x] = keyExpanded[i - (4 - x)];
				temp2[x] = temp1[x] ^ keyExpanded[i - size + x];
			}

			for (k = 0; k < 4; k++){
				keyExpanded[i] = temp2[k];
				i++; 
			}
			
			if (i == 240){
				break;
			}
		}
		
		if (i == 240){
			break;
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////