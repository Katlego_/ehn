#ifndef AES_H
#define AES_H

void decrypt(unsigned char word[][4], unsigned char keyExpanded[240], int size);

void encrypt(unsigned char word[][4], unsigned char keyExpanded[240], int size);

void cypherFeedbackMode();

void electronicCodeBookMode();

#endif