#ifndef FINALROUND_H
#define FINALROUND_H

void decryptFinalRound(unsigned char word[][4], unsigned char keyExpanded[240]);

int encryptFinalRound(unsigned char word[][4], unsigned char keyExpanded[240], int k);

#endif