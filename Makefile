prac2: main.o keyExpansions.o initialRound.o mixingRounds.o finalRound.o print.o AES.o
	gcc main.o keyExpansions.o initialRound.o mixingRounds.o finalRound.o print.o AES.o -static -o prac2 -lm

main.o: main.c
	gcc -c main.c

keyExpansions.o: keyExpansions.c
	gcc -c keyExpansions.c	

initialRound.o: initialRound.c
	gcc -c initialRound.c

mixingRounds.o: mixingRounds.c
	gcc -c mixingRounds.c

finalRound.o: finalRound.c
	gcc -c finalRound.c
	
print.o: print.c
	gcc -c print.c	

AES.o: AES.c
	gcc -c AES.c

clean:
	rm prac2
	rm *.o

run:
	./prac2
	
runAll:
	prac2
	./prac2	